<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Paciente;
use App\Entity\Historia;


class PacienteController extends AbstractController
{
    /**
     * @Route("/paciente", methods={"POST", "GET"}, name="paciente")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function index(Request $request){
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository(Paciente::class)->createQueryBuilder('paciente');
        if ($request->query->has('textBusq')){
            $result = $queryBuilder->select('paciente')
                ->where('paciente.nombre LIKE :textoBusq OR paciente.apellidos LIKE :textoBusq')
                ->setParameter('textoBusq', '%'.$request->query->get('textBusq'). '%')
                ->getQuery()->getResult();

        } else {
            $result = $queryBuilder->select('paciente')->getQuery()->getResult();

        }
        
        return $this->json($result);

    }

    /**
     * @Route("/paciente/crear", methods={"POST"}, name="paciente-crear")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function crear(Request $request){
        try {
            $em = $this->getDoctrine()->getManager();
            $datos = (array) json_decode($request->getContent());
            //$datos = $request->request->all();
            $queryBuilder = $em->getRepository(Paciente::class)->createQueryBuilder('paciente');
            $existePaciente = $queryBuilder->select('paciente')
                ->where('paciente.dni = :dni')
                ->setParameter('dni', $datos['dni'])
                ->getQuery()->getResult();
            if (count($existePaciente) > 0 ){
                throw new \Exception("Ya existe un paciente con el DNI " . $datos['dni']);
            }

            $newPaciente = new Paciente();
            $newPaciente->setApellidos(($datos["apellidos"])? $datos["apellidos"]:'');
            $newPaciente->setCalle(($datos["calle"])? $datos["calle"]:'');
            $newPaciente->setDni(($datos["dni"])? $datos["dni"]:'');
            $newPaciente->setEdad(($datos["edad"])? $datos["edad"]:'');
            $newPaciente->setEmail(($datos["email"])? $datos["email"]:'');
            $newPaciente->setFechaNac(($datos["fecha_nacimiento"])? \Datetime::createFromFormat('d/m/Y',$datos["fecha_nacimiento"]) :'');
            $newPaciente->setMunicipio(($datos["municipio"])? $datos["municipio"]:'');
            $newPaciente->setProfesion(($datos["profesion"])? $datos["profesion"]:'');
            $newPaciente->setProvincia(($datos["provincia"])? $datos["provincia"]:'');
            $newPaciente->setSexo(($datos["sexo"])? $datos["sexo"]:'');
            $newPaciente->setNombre(($datos["nombre"])? $datos["nombre"]:'');

            if ( $datos["foto"]){
                $foto = $this->base64ToImage($datos["foto"],'prueba');
                //var_dump($datos["foto"]);die;
                $newPaciente->setFoto($foto);
            }
            $em->persist($newPaciente);
            $em->flush();
            return $this->json(
                array(
                    'tipo_msg' => 'success',
                    'msg'=>"Se ha guardado correctamente el paciente",
                    'datos'=>array('dni'=>$datos['dni'])
                )
            );
        } catch (\Exception $ex) {
            return $this->json(
                array(
                    'tipo_msg' => 'error',
                    'msg'=>"Se ha producido un error: " . $ex->getMessage(),
                    'datos'=>array('dni'=>$datos)
                )
            );
        }

    }

    function base64ToImage($base64_string, $output_file) {
        $file = fopen($output_file, "wb");
    
        $data = explode(',', $base64_string);
    
        fwrite($file, base64_decode($data[1]));
        fclose($file);
    
        return $output_file;
    }

        /**
     * @Route("/paciente/delete", methods={"DELETE"}, name="delete_paciente")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function delete(Request $request){
        try{
            $datos = (array) json_decode($request->getContent());
            $em = $this->getDoctrine()->getManager();
            $queryBuilder = $em->getRepository(Paciente::class)->createQueryBuilder('paciente');
            $paciente = $queryBuilder->select('paciente')
                ->where('paciente.dni = :dni')
                ->setParameter('dni', $datos['dni'])
                ->getQuery()->getResult();
            if (count($paciente)>0){
                $em->remove($paciente[0]);
                $em->flush();
            }
            //BORRADO DE LAS HISTORIAS DEL PACIENTE QUE SE VA A ELIMINAR
            $queryBuilder = $em->getRepository(Historia::class)->createQueryBuilder('historia');
            $historias_paciente = $queryBuilder->select('historia')
                ->where('historia.paciente = :dni')
                ->setParameter('dni', $datos['dni'])
                ->getQuery()->getResult();

                for($i=0;$i<count($historias_paciente);$i++){
                    $em->remove($historias_paciente[$i]);
                    $em->flush();
                }

            return $this->json(
                array(
                    'tipo_msg' => 'success',
                    'msg'=>"Se ha eliminado correctamente el paciente",
                    'datos'=>array('dni'=>$datos)
                )
            );
        }catch (\Exception $ex) {
            return $this->json(
                array(
                    'tipo_msg' => 'error',
                    'msg'=>"Se ha producido un error: " . $ex->getMessage(),
                    'datos'=>array('dni'=>$datos)
                )
            );
        }

    }

    /**
     * @Route("/paciente/actualizar", methods={"POST"}, name="paciente-actualizar")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function actualizar(Request $request){
        try {
            $em = $this->getDoctrine()->getManager();
            $datos = (array) json_decode($request->getContent());

            if (!empty($foto)) {
                $filename = $foto->getClientOriginalName();
            }
            $queryBuilder = $em->getRepository(Paciente::class)->createQueryBuilder('paciente');
            $existePaciente = $queryBuilder->select('paciente')
                ->where('paciente.dni = :dni')
                ->setParameter('dni', $datos['dni'])
                ->getQuery()->getResult();
                
            if (count($existePaciente) > 0 ){
                $paciente = $existePaciente[0];
                $paciente->setApellidos(($datos["apellidos"])? $datos["apellidos"]:'');
                $paciente->setCalle(($datos["calle"])? $datos["calle"]:'');
                $paciente->setDni(($datos["dni"])? $datos["dni"]:'');
                $paciente->setEdad(($datos["edad"])? $datos["edad"]:'');
                $paciente->setEmail(($datos["email"])? $datos["email"]:'');
                $paciente->setFechaNac(($datos["fecha_nacimiento"])? \Datetime::createFromFormat('d/m/Y',$datos["fecha_nacimiento"]) :'');
                $paciente->setMunicipio(($datos["municipio"])? $datos["municipio"]:'');
                $paciente->setProfesion(($datos["profesion"])? $datos["profesion"]:'');
                $paciente->setProvincia(($datos["provincia"])? $datos["provincia"]:'');
                $paciente->setSexo(($datos["sexo"])? $datos["sexo"]:'');
                $paciente->setNombre(($datos["nombre"])? $datos["nombre"]:'');

                $pos = strpos($datos['foto'], 'base64,');
                $blobData= substr($datos['foto'], $pos + 7);
                $paciente->setFoto($blobData);
                
                $em->persist($paciente);
                $em->flush();
            } else {
                throw new \Exception("El paciente indicado no existe");
            }

            return $this->json(
                array(
                    'tipo_msg' => 'success',
                    'msg'=>"Se ha actualizado correctamente el paciente",
                    'datos'=>array('dni'=>$datos['dni'])
                )
            );
        } catch (\Exception $ex) {
            return $this->json(
                array(
                    'tipo_msg' => 'error',
                    'msg'=>"Se ha producido un error: " . $ex->getMessage(),
                    'datos'=>array('dni'=>$datos)
                )
            );
        }

    }
}
