<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Login;

class AppController extends AbstractController
{
    /**
     * @Route("/appLogin", methods={"POST", "GET"}, name="appLogin")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function login(Request $request)
    {
        try{
            $usuario = $request->query->get('usuario');
            $password = $request->query->get('password');
           
            $passwordFromDDBB='';
            $em = $this->getDoctrine()->getManager();
            $queryBuilder = $em->getRepository(Login::class)->createQueryBuilder('login');
            $result = $queryBuilder->select('login')
                ->where('login.usuario = :usuario')
                ->setParameter('usuario', $usuario)
                ->getQuery()->getResult();
            if(count($result)>0){
                $passwordFromDDBB=$result[0]->getPassword();
            }else{
                return $this->json(
                    array(
                        'tipo_msg' => 'error',
                        'msg'=>"Ese usuario no existe",
                        'datos'=>array('usuario'=>$usuario)
                    )
                );
            }
            if(md5($password)==$passwordFromDDBB){
                return $this->json(
                    array(
                        'tipo_msg' => 'success',
                        'msg'=>"Se ha logeado correctamente",
                        'datos'=>array('usuario'=>$usuario)
                    )
                );
            }else{
                return $this->json(
                    array(
                        'tipo_msg' => 'error',
                        'msg'=>"La contraseña no es correcta",
                        'datos'=>array('usuario'=>$usuario)
                    )
                );
            }
        }catch (\Exception $ex) {
            return $this->json(
                array(
                    'tipo_msg' => 'error',
                    'msg'=>"Se ha producido un error: " . $ex->getMessage(),
                    'datos'=>array('usuario'=>$usuario)
                )
            );
        }

    }
}