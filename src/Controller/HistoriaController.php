<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Historia;

class HistoriaController extends AbstractController
{
    /**
     * @Route("/historia-paciente/{id_paciente}", methods={"POST", "GET"}, name="historia-paciente")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function index(Request $request)
    {
        $id_paciente = $request->attributes->get('id_paciente');
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository(Historia::class)->createQueryBuilder('historia');
        $result = $queryBuilder->select('historia')
            ->where('historia.paciente = :dni')
            ->setParameter('dni', $id_paciente)
            ->getQuery()->getResult();
        return $this->json($result);

    }

    /**
     * @Route("/historia/crear", methods={"POST"}, name="historia-crear")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function crear(Request $request){
        try {
            $em = $this->getDoctrine()->getManager();
            $datos = (array) json_decode($request->getContent());
            $newHistoria = new Historia();
            $newHistoria->setAf(($datos["af"])? $datos["af"]:'');
            $newHistoria->setAlergias(($datos["alergias"])? $datos["alergias"]:'');
            //$newHistoria->setAnamnesis(($datos["anamnesis"])? $datos["anamnesis"]:'');
            $newHistoria->setComplementaria(($datos["complementaria"])? $datos["complementaria"]:'');
            $newHistoria->setDiagnostico(($datos["diagnostico"])? $datos["diagnostico"]:'');
            $newHistoria->setFecha(($datos["fecha"])? \Datetime::createFromFormat('d/m/Y',$datos["fecha"]) :'');
            $newHistoria->setTratamiento(($datos["tratamiento"])? $datos["tratamiento"]:'');
            $newHistoria->setMasaCorporal(($datos["masa_corporal"])? $datos["masa_corporal"]:'');
            //$newHistoria->setExploracion(($datos["exploracion"])? $datos["exploracion"]:'');
            $newHistoria->setPaciente(($datos["paciente"])? $datos["paciente"]:'');
            $em->persist($newHistoria);
            $em->flush();
            return $this->json(
                array(
                    'tipo_msg' => 'success',
                    'msg'=>"Se ha añadido correctamente al historial",
                    'datos'=>array('id'=>$newHistoria->getId())
                )
            );
        } catch (\Exception $ex) {
            return $this->json(
                array(
                    'tipo_msg' => 'error',
                    'msg'=>"Se ha producido un error: " . $ex->getMessage(),
                    'datos'=>array('datos'=>$datos)
                )
            );
        }

    }

    /**
     * @Route("/historia/delete", methods={"DELETE"}, name="historia-delete")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function delete(Request $request){
        try{
            $em = $this->getDoctrine()->getManager();
            $datos = (array) json_decode($request->getContent());
            $queryBuilder = $em->getRepository(Historia::class)->createQueryBuilder('historia');

            $historia = $queryBuilder->select('historia')
                ->where('historia.id = :id')
                ->setParameter('id', $datos['id'])
                ->getQuery()->getResult();
            if(count($historia)>0){
                $em->remove($historia[0]);
                $em->flush();
            }
            return  $this->json(array(
                'tipo_msg' => 'success',
                'msg'=>"Se ha eliminado correctamente la historia",
                'datos'=>array('id'=>$datos['id'])
                )
            );
        }catch (\Exception $ex) {
            return $this->json(
                array(
                    'tipo_msg' => 'error',
                    'msg'=>"Se ha producido un error: " . $ex->getMessage(),
                    'datos'=>array('id'=>$datos)
                )
            );
    }
            
    }

    /**
     * @Route("/historia/actualizar", methods={"POST"}, name="historia-actualizar")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function actualizar(Request $request){
        try {
            $em = $this->getDoctrine()->getManager();
            $datos = (array) json_decode($request->getContent());

            $queryBuilder = $em->getRepository(Historia::class)->createQueryBuilder('historia');
            $existeHistoria = $queryBuilder->select('historia')
                ->where('historia.id = :id')
                ->setParameter('id', $datos['id'])
                ->getQuery()->getResult();
                
            if (count($existeHistoria) > 0 ){
                $historia = $existeHistoria[0];
                $historia->setAf(($datos["af"])? $datos["af"]:'');
                $historia->setAlergias(($datos["alergias"])? $datos["alergias"]:'');
                $historia->setMasaCorporal(($datos["masa_corporal"])? $datos["masa_corporal"]:'');
                //$historia->setAnamnesis(($datos["anamnesis"])? $datos["anamnesis"]:'');
                //$historia->setExploracion(($datos["exploracion"])? $datos["exploracion"]:'');
                $historia->setFecha(($datos["fecha"])? \Datetime::createFromFormat('d/m/Y',$datos["fecha"]) :'');
                $historia->setComplementaria(($datos["complementaria"])? $datos["complementaria"]:'');
                $historia->setDiagnostico(($datos["diagnostico"])? $datos["diagnostico"]:'');
                $historia->setTratamiento(($datos["tratamiento"])? $datos["tratamiento"]:'');
                //$historia->setPaciente(($datos["paciente"])? $datos["paciente"]:'');
                $em->persist($historia);
                $em->flush();
            } else {
                throw new \Exception("Esa historia no existe");
            }

            return $this->json(
                array(
                    'tipo_msg' => 'success',
                    'msg'=>"Se ha actualizado correctamente la historia",
                    //'datos'=>array('paciente'=>$datos['paciente'])
                    'datos'=>array('id'=>$datos['id'])
                )
            );
        } catch (\Exception $ex) {
            return $this->json(
                array(
                    'tipo_msg' => 'error',
                    'msg'=>"Se ha producido un error: " . $ex->getMessage(),
                    'datos'=>array('id'=>$datos)
                )
            );
        }

    }
}