<?php

namespace App\Entity;

use App\Repository\AnamnesisRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AnamnesisRepository::class)
 */
class Anamnesis
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cardiovasculatorio;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $orl;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $rinon_vu;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $piel;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $locomotor;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $boca;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $hematologia;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tratamientos;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $respiratorio;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $digestivo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $genital;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $s_nervioso_central;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $oftalmologico;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $otros;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCardiovasculatorio(): ?string
    {
        return $this->cardiovasculatorio;
    }

    public function setCardiovasculatorio(?string $cardiovasculatorio): self
    {
        $this->cardiovasculatorio = $cardiovasculatorio;

        return $this;
    }

    public function getOrl(): ?string
    {
        return $this->orl;
    }

    public function setOrl(?string $orl): self
    {
        $this->orl = $orl;

        return $this;
    }

    public function getRinonVu(): ?string
    {
        return $this->rinon_vu;
    }

    public function setRinonVu(?string $rinon_vu): self
    {
        $this->rinon_vu = $rinon_vu;

        return $this;
    }

    public function getPiel(): ?string
    {
        return $this->piel;
    }

    public function setPiel(?string $piel): self
    {
        $this->piel = $piel;

        return $this;
    }

    public function getLocomotor(): ?string
    {
        return $this->locomotor;
    }

    public function setLocomotor(?string $locomotor): self
    {
        $this->locomotor = $locomotor;

        return $this;
    }

    public function getBoca(): ?string
    {
        return $this->boca;
    }

    public function setBoca(?string $boca): self
    {
        $this->boca = $boca;

        return $this;
    }

    public function getHematologia(): ?string
    {
        return $this->hematologia;
    }

    public function setHematologia(?string $hematologia): self
    {
        $this->hematologia = $hematologia;

        return $this;
    }

    public function getTratamientos(): ?string
    {
        return $this->tratamientos;
    }

    public function setTratamientos(?string $tratamientos): self
    {
        $this->tratamientos = $tratamientos;

        return $this;
    }

    public function getRespiratorio(): ?string
    {
        return $this->respiratorio;
    }

    public function setRespiratorio(?string $respiratorio): self
    {
        $this->respiratorio = $respiratorio;

        return $this;
    }

    public function getDigestivo(): ?string
    {
        return $this->digestivo;
    }

    public function setDigestivo(?string $digestivo): self
    {
        $this->digestivo = $digestivo;

        return $this;
    }

    public function getGenital(): ?string
    {
        return $this->genital;
    }

    public function setGenital(?string $genital): self
    {
        $this->genital = $genital;

        return $this;
    }

    public function getSNerviosoCentral(): ?string
    {
        return $this->s_nervioso_central;
    }

    public function setSNerviosoCentral(?string $s_nervioso_central): self
    {
        $this->s_nervioso_central = $s_nervioso_central;

        return $this;
    }

    public function getOftalmologico(): ?string
    {
        return $this->oftalmologico;
    }

    public function setOftalmologico(?string $oftalmologico): self
    {
        $this->oftalmologico = $oftalmologico;

        return $this;
    }

    public function getOtros(): ?string
    {
        return $this->otros;
    }

    public function setOtros(?string $otros): self
    {
        $this->otros = $otros;

        return $this;
    }
}
