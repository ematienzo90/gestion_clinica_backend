<?php

namespace App\Entity;

use App\Repository\PacienteRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Types\BlobType;
use Doctrine\DBAL\Types\TextType;

/**
 * @ORM\Entity(repositoryClass=PacienteRepository::class)
 */
class Paciente
{

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $nombre;

    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=10)
     */
    private $dni;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $apellidos;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $edad;

    /**
     * @ORM\Column(type="string", length=6, nullable=true)
     */
    private $sexo;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $profesion;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $calle;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $municipio;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $provincia;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $fecha_nac;

       /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\OneToMany(targetEntity="Historia", mappedBy="paciente")
     */
    private $historias;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $foto;

    public function __construct() {
        $this->historias = new ArrayCollection();
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getDni(): ?string
    {
        return $this->dni;
    }

    public function setDni(string $dni): self
    {
        $this->dni = $dni;

        return $this;
    }

    public function getApellidos(): ?string
    {
        return $this->apellidos;
    }

    public function setApellidos(string $apellidos): self
    {
        $this->apellidos = $apellidos;

        return $this;
    }

    public function getEdad(): ?int
    {
        return $this->edad;
    }

    public function setEdad(?int $edad): self
    {
        $this->edad = $edad;

        return $this;
    }

    public function getSexo(): ?string
    {
        return $this->sexo;
    }

    public function setSexo(?string $sexo): self
    {
        $this->sexo = $sexo;

        return $this;
    }

    public function getProfesion(): ?string
    {
        return $this->profesion;
    }

    public function setProfesion(string $profesion): self
    {
        $this->profesion = $profesion;

        return $this;
    }

    public function getCalle(): ?string
    {
        return $this->calle;
    }

    public function setCalle(?string $calle): self
    {
        $this->calle = $calle;

        return $this;
    }

    public function getMunicipio(): ?string
    {
        return $this->municipio;
    }

    public function setMunicipio(?string $municipio): self
    {
        $this->municipio = $municipio;

        return $this;
    }

    public function getProvincia(): ?string
    {
        return $this->provincia;
    }

    public function setProvincia(?string $provincia): self
    {
        $this->provincia = $provincia;

        return $this;
    }

    public function getFechaNac(): ?string
    {   
        return ($this->fecha_nac !== null)? $this->fecha_nac->format('d/m/Y') : '';
    }

    public function setFechaNac(?\DateTime $fecha_nac): self
    {
        $this->fecha_nac=$fecha_nac;
        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getHistorias(): ?Array
    {
        return array();
        //return $this->historias;
    }

    public function getFoto(): ?string
    {
        return 'data:image/jpeg;base64,'.$this->foto;
    }

    public function setFoto(?string $foto): self
    {
        $this->foto = $foto;

        return $this;
    }

}
