<?php

namespace App\Entity;

use App\Repository\LoginRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass=LoginRepository::class)
 */
class Login
{

    /**
     * @ORM\Column(type="string", length=15)
     * @ORM\Id()
     */
    private $usuario;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $password;


    public function getUsuario(): ?string
    {
        return $this->provincia;
    }

    public function setUsuario(?string $usuario): self
    {
        $this->usuario = $usuario;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }
}