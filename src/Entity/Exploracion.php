<?php

namespace App\Entity;

use App\Repository\ExploracionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ExploracionRepository::class)
 */
class Exploracion
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $eg;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $temperatura;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $piel;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $craneo_cuello;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $boca_orl;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $torax;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $otros;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEg(): ?string
    {
        return $this->eg;
    }

    public function setEg(?string $eg): self
    {
        $this->eg = $eg;

        return $this;
    }

    public function getTemperatura(): ?float
    {
        return $this->temperatura;
    }

    public function setTemperatura(?float $temperatura): self
    {
        $this->temperatura = $temperatura;

        return $this;
    }

    public function getPiel(): ?string
    {
        return $this->piel;
    }

    public function setPiel(?string $piel): self
    {
        $this->piel = $piel;

        return $this;
    }

    public function getCraneoCuello(): ?string
    {
        return $this->craneo_cuello;
    }

    public function setCraneoCuello(?string $craneo_cuello): self
    {
        $this->craneo_cuello = $craneo_cuello;

        return $this;
    }

    public function getBocaOrl(): ?string
    {
        return $this->boca_orl;
    }

    public function setBocaOrl(?string $boca_orl): self
    {
        $this->boca_orl = $boca_orl;

        return $this;
    }

    public function getTorax(): ?string
    {
        return $this->torax;
    }

    public function setTorax(?string $torax): self
    {
        $this->torax = $torax;

        return $this;
    }

    public function getOtros(): ?string
    {
        return $this->otros;
    }

    public function setOtros(?string $otros): self
    {
        $this->otros = $otros;

        return $this;
    }
}
