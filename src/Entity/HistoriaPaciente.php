<?php

namespace App\Entity;

use App\Repository\HistoriaPacienteRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=HistoriaPacienteRepository::class)
 */
class HistoriaPaciente
{
    /**
     *  @ORM\Id()
     * @ORM\Column(type="string", length=255)
     *
     */
    private $id_paciente;

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     */
    private $id_historia;

    public function getIdPaciente(): ?string
    {
        return $this->id_paciente;
    }

    public function setIdPaciente(string $id_paciente): self
    {
        $this->id_paciente = $id_paciente;

        return $this;
    }

    public function getIdHistoria(): ?int
    {
        return $this->id_historia;
    }

    public function setIdHistoria(int $id_historia): self
    {
        $this->id_historia = $id_historia;

        return $this;
    }
}
