<?php

namespace App\Entity;

use App\Repository\HistoriaRepository;
use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\Types\Integer;

/**
 * @ORM\Entity(repositoryClass=HistoriaRepository::class)
 */
class Historia
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $af;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $alergias;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $masa_corporal;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $anamnesis;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $exploracion;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $complementaria;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $diagnostico;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tratamiento;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $fecha;

    /**
     * @ORM\Column(type="string", length=10)
     * @ORM\ManyToOne(
     *      targetEntity="Paciente",
     *      inversedBy="historias"
     * )
     */
    private $paciente;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAf(): ?string
    {
        return $this->af;
    }

    public function setAf(?string $af): self
    {
        $this->af = $af;

        return $this;
    }

    public function getAlergias(): ?string
    {
        return $this->alergias;
    }

    public function setAlergias(?string $alergias): self
    {
        $this->alergias = $alergias;

        return $this;
    }

    public function getMasaCorporal(): ?string
    {
        return $this->masa_corporal;
    }

    public function setMasaCorporal(?string $masa_corporal): self
    {
        $this->masa_corporal = $masa_corporal;

        return $this;
    }

    public function getAnamnesis(): ?int
    {
        return $this->anamnesis;
    }

    public function setAnamnesis(?int $anamnesis): self
    {
        $this->anamnesis = $anamnesis;

        return $this;
    }

    public function getExploracion(): ?integer
    {
        return $this->exploracion;
    }

    public function setExploracion(?integer $exploracion): self
    {
        $this->exploracion = $exploracion;

        return $this;
    }

    public function getComplementaria(): ?string
    {
        return $this->complementaria;
    }

    public function setComplementaria(?string $complementaria): self
    {
        $this->complementaria = $complementaria;

        return $this;
    }

    public function getDiagnostico(): ?string
    {
        return $this->diagnostico;
    }

    public function setDiagnostico(?string $diagnostico): self
    {
        $this->diagnostico = $diagnostico;

        return $this;
    }

    public function getTratamiento(): ?string
    {
        return $this->tratamiento;
    }

    public function setTratamiento(?string $tratamiento): self
    {
        $this->tratamiento = $tratamiento;

        return $this;
    }

    public function getFecha(): ?string
    {
        return ($this->fecha !== null)? $this->fecha->format('d/m/Y') : '';
    }

    public function setFecha(?\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function setPaciente(?string $paciente): self
    {
        $this->paciente = $paciente;

        return $this;
    }

    // public function getPaciente(): ?\Paciente
    // {
    //     return $this->paciente;
    // } 
}
