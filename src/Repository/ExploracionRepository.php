<?php

namespace App\Repository;

use App\Entity\Exploracion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Exploracion|null find($id, $lockMode = null, $lockVersion = null)
 * @method Exploracion|null findOneBy(array $criteria, array $orderBy = null)
 * @method Exploracion[]    findAll()
 * @method Exploracion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExploracionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Exploracion::class);
    }

    // /**
    //  * @return Exploracion[] Returns an array of Exploracion objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Exploracion
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
