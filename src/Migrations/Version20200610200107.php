<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200610200107 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE anamnesis ADD cardiovasculatorio VARCHAR(255) DEFAULT NULL, ADD respiratorio VARCHAR(255) DEFAULT NULL, DROP cardiocirculatorio, DROP respiratotio, DROP endocrino, CHANGE id id INT AUTO_INCREMENT NOT NULL');
        $this->addSql('ALTER TABLE exploracion CHANGE id id INT AUTO_INCREMENT NOT NULL');
        $this->addSql('ALTER TABLE historia CHANGE diagnosticos diagnostico VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE historia_paciente DROP FOREIGN KEY fk_id_historia');
        $this->addSql('ALTER TABLE historia_paciente DROP FOREIGN KEY fk_id_paciente');
        $this->addSql('DROP INDEX fk_id_historia ON historia_paciente');
        $this->addSql('DROP INDEX IDX_A68EC99C961045CB ON historia_paciente');
        $this->addSql('ALTER TABLE historia_paciente CHANGE id_paciente id_paciente VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE anamnesis ADD cardiocirculatorio VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_0900_ai_ci`, ADD respiratotio VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_0900_ai_ci`, ADD endocrino VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_0900_ai_ci`, DROP cardiovasculatorio, DROP respiratorio, CHANGE id id INT NOT NULL');
        $this->addSql('ALTER TABLE exploracion CHANGE id id INT NOT NULL');
        $this->addSql('ALTER TABLE historia CHANGE diagnostico diagnosticos VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_0900_ai_ci`');
        $this->addSql('ALTER TABLE historia_paciente CHANGE id_paciente id_paciente VARCHAR(10) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_0900_ai_ci`');
        $this->addSql('ALTER TABLE historia_paciente ADD CONSTRAINT fk_id_historia FOREIGN KEY (id_historia) REFERENCES historia (id) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('ALTER TABLE historia_paciente ADD CONSTRAINT fk_id_paciente FOREIGN KEY (id_paciente) REFERENCES paciente (dni) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('CREATE INDEX fk_id_historia ON historia_paciente (id_historia)');
        $this->addSql('CREATE INDEX IDX_A68EC99C961045CB ON historia_paciente (id_paciente)');
    }
}
