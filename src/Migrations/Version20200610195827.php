<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200610195827 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE anamnesis (id INT AUTO_INCREMENT NOT NULL, cardiovasculatorio VARCHAR(255) DEFAULT NULL, orl VARCHAR(255) DEFAULT NULL, rinon_vu VARCHAR(255) DEFAULT NULL, piel VARCHAR(255) DEFAULT NULL, locomotor VARCHAR(255) DEFAULT NULL, boca VARCHAR(255) DEFAULT NULL, hematologia VARCHAR(255) DEFAULT NULL, tratamientos VARCHAR(255) DEFAULT NULL, respiratorio VARCHAR(255) DEFAULT NULL, digestivo VARCHAR(255) DEFAULT NULL, genital VARCHAR(255) DEFAULT NULL, s_nervioso_central VARCHAR(255) DEFAULT NULL, oftalmologico VARCHAR(255) DEFAULT NULL, otros VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE exploracion (id INT AUTO_INCREMENT NOT NULL, eg VARCHAR(255) DEFAULT NULL, temperatura DOUBLE PRECISION DEFAULT NULL, piel VARCHAR(255) DEFAULT NULL, craneo_cuello VARCHAR(255) DEFAULT NULL, boca_orl VARCHAR(255) DEFAULT NULL, torax VARCHAR(255) DEFAULT NULL, otros VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE historia (id INT AUTO_INCREMENT NOT NULL, af VARCHAR(255) DEFAULT NULL, alergias VARCHAR(255) DEFAULT NULL, masa_corporal VARCHAR(255) DEFAULT NULL, anamnesis INT DEFAULT NULL, exploracion INT DEFAULT NULL, complementaria VARCHAR(255) DEFAULT NULL, diagnostico VARCHAR(255) DEFAULT NULL, tratamiento VARCHAR(255) DEFAULT NULL, fecha DATE DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE historia_paciente (id_paciente VARCHAR(255) NOT NULL, id_historia INT NOT NULL, PRIMARY KEY(id_paciente, id_historia)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE paciente (dni VARCHAR(10) NOT NULL, nombre VARCHAR(20) NOT NULL, apellidos VARCHAR(50) NOT NULL, edad INT DEFAULT NULL, sexo VARCHAR(6) DEFAULT NULL, profesion VARCHAR(30) DEFAULT NULL, calle VARCHAR(50) DEFAULT NULL, municipio VARCHAR(30) DEFAULT NULL, provincia VARCHAR(20) DEFAULT NULL, fecha_nac DATE DEFAULT NULL, PRIMARY KEY(dni)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE anamnesis');
        $this->addSql('DROP TABLE exploracion');
        $this->addSql('DROP TABLE historia');
        $this->addSql('DROP TABLE historia_paciente');
        $this->addSql('DROP TABLE paciente');
    }
}
